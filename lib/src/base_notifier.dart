import 'package:flutter/material.dart';

/*
 * add mounted option to stop calling build methods after disposal
 */
enum NotifierState { loading, loaded, error }

class BaseNotifier extends ChangeNotifier {
  NotifierState _state = NotifierState.loading;
  bool _mounted = true;

  NotifierState get state => _state;
  bool get busy => _state == NotifierState.loading;
  bool get hasError => _state == NotifierState.error;
  bool get mounted => _mounted;

  void setState({NotifierState state}) {
    if (state != null) _state = state;

    if (mounted) notifyListeners();
  }

  @override
  void dispose() {
    _mounted = false;
    super.dispose();
  }
}
